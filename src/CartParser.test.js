import { v4 as uuidV4 } from 'uuid';
import fs from 'fs';
import CartParser from './CartParser';

let parser;

jest.mock('uuid');

beforeEach(() => {
  parser = new CartParser();
  uuidV4.mockImplementation(() => 'testId');
});

describe('CartParser - unit tests', () => {
  describe('validate', () => {
    it('should return array without errors', () => {
      const csv = `Product name,Price,Quantity
			Mollis consequat,9.00,2`;

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(0);
    });

    it('should return array with 3 errors in header(missing name 1 column in header)', () => {
      const csv = `Price,Quantity
			Mollis consequat,9.00,2`;
      const errorOne = {
        type: 'header',
        row: 0,
        column: 0,
        message: 'Expected header to be named "Product name" but received Price.',
      };
      const errorTwo = {
        type: 'header',
        row: 0,
        column: 1,
        message: 'Expected header to be named "Price" but received Quantity.',
      };
      const errorThree = {
        type: 'header',
        row: 0,
        column: 2,
        message: 'Expected header to be named "Quantity" but received undefined.',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(3);
      expect(errors[0]).toEqual(errorOne);
      expect(errors[1]).toEqual(errorTwo);
      expect(errors[2]).toEqual(errorThree);
    });

    it('should return array with 1 error in header(wrong name column)', () => {
      const csv = `Product name,Test,Quantity
			Mollis consequat,9.00,2`;
      const error = {
        type: 'header',
        row: 0,
        column: 1,
        message: 'Expected header to be named "Price" but received Test.',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(1);
      expect(errors[0]).toEqual(error);
    });

    it('should return array with 1 error in row(cells.length < 3)', () => {
      const csv = `Product name,Price,Quantity
			Mollis consequat,9.00`;
      const result = {
        type: 'row',
        row: 1,
        column: -1,
        message: 'Expected row to have 3 cells but received 2.',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(1);
      expect(errors[0]).toEqual(result);
    });

    it('should return array with 1 error in cell(name product is empty)', () => {
      const csv = `Product name,Price,Quantity
			,9.00,2`;
      const result = {
        type: 'cell',
        row: 1,
        column: 0,
        message: 'Expected cell to be a nonempty string but received "".',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(1);
      expect(errors[0]).toEqual(result);
    });

    it('should return array with 1 error in cell(price is string)', () => {
      const csv = `Product name,Price,Quantity
			Mollis consequat,test,2`;
      const result = {
        type: 'cell',
        row: 1,
        column: 1,
        message: 'Expected cell to be a positive number but received "test".',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(1);
      expect(errors[0]).toEqual(result);
    });

    it('should return array with 2 error in cell(price and quantity is negative number)', () => {
      const csv = `Product name,Price,Quantity
			Mollis consequat,-2,-3`;
      const errorOne = {
        type: 'cell',
        row: 1,
        column: 1,
        message: 'Expected cell to be a positive number but received "-2".',
      };
      const errorTwo = {
        type: 'cell',
        row: 1,
        column: 2,
        message: 'Expected cell to be a positive number but received "-3".',
      };

      const errors = parser.validate(csv);

      expect(errors).toHaveLength(2);
      expect(errors[0]).toEqual(errorOne);
      expect(errors[1]).toEqual(errorTwo);
    });
  });

  describe('parseLine', () => {
    it('should return an obj such us { id: "testId", name: string, price: number, quantity: number }', () => {
      const cart = 'Mollis consequat,9.00,2';
      const result = {
        id: 'testId',
        name: 'Mollis consequat',
        price: 9,
        quantity: 2,
      };

      const item = parser.parseLine(cart);

      expect(item).toEqual(result);
    });
  });

  describe('calcTotal', () => {
    it('should return total price', () => {
      const cart = [
        {
          price: 9,
          quantity: 2,
        },
        { price: 2, quantity: 3 },
      ];
      const result = 24;

      expect(parser.calcTotal(cart)).toEqual(result);
    });
  });

  describe('readFile', () => {
    it('should return content from file', () => {
      const path = './samples/cart.csv';
      const result = 'Mollis consequat,9.00,2';

      const content = parser.readFile(path);

      expect(content).toMatch(result);
    });
  });

  describe('parse', () => {
    it('should throw error', () => {
      jest.spyOn(fs, 'readFileSync');
      jest.spyOn(console, 'error');
      fs.readFileSync.mockImplementation(
        () => `Product name,Price,Quantity
								Mollis consequat,9.00,2
								Tvoluptatem,10.32`
      );
      console.error.mockImplementation(() => {});

      expect(() => {
        parser.parse('path');
      }).toThrow('Validation failed!');

      console.error.mockRestore();
      fs.readFileSync.mockRestore();
    });
  });
});

describe('CartParser - integration test', () => {
  it('should return a object with cart items and total price', () => {
    jest.spyOn(fs, 'readFileSync');
    fs.readFileSync.mockImplementation(
      () => `Product name,Price,Quantity
								Mollis consequat,9.00,2
								Tvoluptatem,10.32,1`
    );

    const result = {
      items: [
        {
          id: 'testId',
          name: 'Mollis consequat',
          price: 9,
          quantity: 2,
        },
        {
          id: 'testId',
          name: 'Tvoluptatem',
          price: 10.32,
          quantity: 1,
        },
      ],
      total: 28.32,
    };

    const parsCart = parser.parse('path');

    expect(parsCart).toEqual(result);

    fs.readFileSync.mockRestore();
  });
});
